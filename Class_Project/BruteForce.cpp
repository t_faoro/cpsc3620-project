/**
* Filename: BruteForce.cpp
* Purpose: Simply implements the Brute Force algorithm located within BruteForce.h
*
* Preconditions: Inclusion of BruteForce.h
*
* Output: The Closest Pair of coordinates, outputted to the console.
* Contributors: Eric Panich 	- eric.panich@uleth.ca
*				Tylor Faoro 	- tylor.faoro@uleth.ca
*				Juan Monterrosa - juan.monterrosa@uleth.ca
*/
#include <vector>		// For Vector Data Structure
#include <math.h>		// For sqrt() and abs()
#include <iostream>		// For cout
#include <fstream>		// For ofstream
#include <string>		// For to_string()
#include "BruteForce.h"

using namespace std;

int main(){
	//:: Declares Coordinates to become ordered pairs
		vector<int> test = bruteForce();
		vector<int> falseCoord = {0,0,0,0}; 

		if (equal (test.begin(), test.end(), falseCoord.begin()) ) {
			cout << "No results." << endl; 
		}
		else {
			cout << "The shortest distance between 2 points are found at : " << endl;
			cout << "Index " << test[2] << " : " << " ( " << test[0] << " , " << test[1] << " ) " << endl << "And" << endl;
			cout << "Index " << test[5] << " : " << " ( " << test[3] << " , " << test[4] << " ) " << endl;
		}
			
	return 0;

}
