#include <vector>		// For Vector Data Structure
#include <iostream>		// For cout
#include <fstream>		// For ofstream
#include <string>		// For to_string()
#include <utility>		// For pair , make_pair
#include <climits>		// for INT_MAX

using namespace std;


void create(vector<int> x, vector<int> y, int size);

int main(){
	//:: Declares Coordinates to become ordered pairs
	vector<int> xCoord;
	vector<int> yCoord;

//		system("pause");
	


	//:: Declares our universal size
	int n = 0;

	//:: Prompts user for input. We are assuming they are smart.
	cout << "Enter the number of inputs (int): ";
	cin >> n;
	cout << endl;

	//:: Showing the number od coordinates made
	cout << "Number of Coordinates Created: " << n << endl;

	//:: Push all data into its respective vector
	for (int i = 0; i < ( n ); i++){
		srand(time_t(i - 0));
		xCoord.push_back((((rand() * (rand() + rand())) % 2000000) - 1000000) / ((rand() % 2) + 1));

		srand(time_t((i - 1) - 0));
		yCoord.push_back((((rand() * (rand() + rand())) % 2000000) - 1000000) / ((rand() % 2) + 1));
	}

	//:: Run Create, which pairs the vectors and generates the filename
	create(xCoord, yCoord, n);
	cout << endl;

	//:: Pause so you can see all above.
//	system("pause");

}

/**
* Purpose: To create ordered pairs from two vectors using a user stated input
* Preconditions:
*		User must input a valid int value. The values must already be placed into the respective vectors.
* PostConditions:
*		File out outputted to the main directory where the program lives. The file is populated with the ordered
*		pairs. The file is appended with the number of data items and the file format *.txt. The final output file
*		will be Output<n>.txt.
*
* Author: Tylor Faoro - tylor.faoro@uleth.ca
*/
void create(vector<int> x, vector<int> y, int size){

	string filename = "Output" + to_string(size) + ".txt";		//:: Creates Filename called "Output" with the number of tests appended.
	ofstream outFile(filename/*, ios::app*/);						//:: Creates file to be manipulated
	//filename.append(".txt");									//:: Appends .txt to the end of the file.
	int n = size;
	
	//:: Test Code - Declaring pair variable
//	std::pair<int, int> indices;

	outFile << to_string(size) << endl;
	
	for (int i = 0; i < ( n ); i++){
		outFile << x[i] << " " << y[i] << endl;
		
//		//:: Test Code - Using the Pair Class to try to return cartesian products
//		indices = make_pair(x[i], y[i]);
//		cout << indices.first << " " << indices.second << endl;
	}

	outFile.close();

}
