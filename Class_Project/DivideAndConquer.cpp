#include <vector>		// For Vector Data Structure
#include <math.h>		// For sqrt() and abs()
#include <iostream>		// For cout
#include <fstream>		// For ofstream
#include <string>		// For to_string()
#include <algorithm>		// For Min
#include <chrono>		// For time_point (Run-time clock)

using namespace std;

//:: Function Headers
vector<int> mergeSort(vector<int> unsortedArray);
vector<int> merge(vector<int> left, vector<int> right);
void sortedPair(vector<int> sortedArray, vector<int> unsortedArray, chrono::time_point<chrono::system_clock> timeStart, chrono::time_point<chrono::system_clock> timeEnd);

//:: Main function
int main(){

	string filename;
	int size, xCoord, yCoord;
	chrono::time_point<chrono::system_clock> timeStart, timeEnd;

	vector<int> A = {};

	cout << "Enter the file you wish to test (Example: Output2000.txt) : ";
	getline(cin, filename);
	
	ifstream fin;
	fin.open(filename);

	if (!fin.good()) {	
		cout << "bad filename" << endl;
		cout << "No results." << endl; 
		return 0;
	}
	
	fin >> size;
	
	int k = 0;
	while (!fin.eof() && k < ( size * 2 ) ){
		fin >> xCoord;
		fin >> yCoord;

		A.push_back(xCoord);
		A.push_back(yCoord);
		k += 2;
	}	

	timeStart = chrono::system_clock::now();

	vector<int> sortedArray = mergeSort(A);

	sortedPair(sortedArray, A, timeStart, timeEnd);

	return 0;
}

/**
* Purpose: Provides a O(n log n) time sort to be used on our vector of ints. How a merge sort works will be assumed an axiom.
* 
* PreConditions: A generated set of data that has been inserted into a vector of ints. 
*
* @Param: vector<int> unsortedArray - The unsorted vector data to be sorted.
*/
vector<int> mergeSort( vector<int> unsortedArray ){
	vector<int> Left;
	vector<int> Right;
	vector<int> Result;

	int middle = (int)unsortedArray.size() / 2;

	if( middle <= 1){
		return unsortedArray;
	}

	if (middle % 2 != 0 )
		middle++;

	for (int i = 0; i < middle; i++){
		Left.push_back(unsortedArray[i]);	
	}

	for (int i = middle; i < (int)unsortedArray.size(); i++){
		Right.push_back(unsortedArray[i]);	
	}
	
	Left = mergeSort(Left);
	Right = mergeSort(Right);
	Result = merge(Left, Right);
	
	return Result;

}
/**
* Purpose: To merge sorted data from the merge sort algorithm. Will take any two vectors of ints and merge them together.
*
* @param: vector<int> left - When data has been split, this vector takes the left side data (begin,...,n/2).
* @param: vector<int> right - When data has been split, this vector takes the right side data (n/2,...,end).
*/
vector<int> merge(vector<int> left, vector<int> right){

	vector<int> sorted;

		while( left.size() > 0 && right.size() > 0){
			
			if( left[0] + left[1] <= right[0] + right[1] ){
				sorted.push_back(left.front());
				left.erase(left.begin());
				sorted.push_back(left.front());
				left.erase(left.begin());
			}

			else{
				sorted.push_back(right.front() );
				right.erase(right.begin() );			
				sorted.push_back(right.front() );
				right.erase(right.begin() );			

			}
		}
		
		if ( left.size() == 0 ){
			while (right.size() > 0) {
				sorted.push_back(right.front() );
				right.erase(right.begin() );

			}
		}
		if ( right.size() == 0 ){
			while (left.size() > 0) {
				sorted.push_back(left.front());
				left.erase(left.begin());

			}
		}
	return sorted;

}
/**
* Purpose: Acts as our closest pair finder. This is the engine of the entire algorithm.
*
* PreConditions: vector<int> sortedArray has been presorted by the provided merge sort algorithm.
*
* @Param : vector<int> sortedArray 	 - Presorted Data that will be checked for our closest pair. Presorted data will make the algorithm more efficient.
* @Param : vector<int> unsortedArray - Is used to provide the original index location of the closest pairs.
* @Param : chrono::time_point<chrono::system_clock> timeStart - Provides a start clock to time the algorithm.
* @Param : chrono::time_point<chrono::system_clock> timeEnd	  - Provides an end clock to time the algorithm.
*/
void sortedPair(vector<int> sortedArray, vector<int> unsortedArray, chrono::time_point<chrono::system_clock> timeStart, chrono::time_point<chrono::system_clock> timeEnd){

	int checkDistance = 2000000, index = 0, unsortedIndex1 = 0, unsortedIndex2 = 0;

	for (int i = 0; i < (int)sortedArray.size(); i += 2) {

		int absoluteDistance = ( abs( abs( sortedArray[i] ) - abs( sortedArray[i+2]) ) + abs( abs( sortedArray[i+1]) - abs( sortedArray[i+3]) ) );

		if ( absoluteDistance < checkDistance) {
			checkDistance = absoluteDistance;
			index = i;
		}
	}

	//Find which Index in unsorted verion:
	for (int i = 0; i < (int)unsortedArray.size() && (unsortedIndex1 == 0 || unsortedIndex2 == 0); i += 2) {
			if (sortedArray[index] == unsortedArray[i] && sortedArray[index+1] == unsortedArray[i+1])
				unsortedIndex1 = i/2;

			if (sortedArray[index+2] == unsortedArray[i] && sortedArray[index+3] == unsortedArray[i+1])
				unsortedIndex2 = i/2;
	}

	//:: End Run-Time clock
	timeEnd = chrono::system_clock::now();

	//:: Calculate how long the program ran
	chrono::duration<double> programDuration = timeEnd - timeStart;
	
	//:: Output program Run-Time.
	cout << "Program \"Wall Time\" Duration: " << programDuration.count() << " seconds." << endl << endl; 
	
	cout << "The shortest distance between 2 points are found at : " << endl;
	cout << "Index " << unsortedIndex1 << " : " << " ( " << sortedArray[index] << " , " << sortedArray[index+1] << " ) " << endl << "And" << endl;
	cout << "Index " << unsortedIndex2 << " : " << " ( " << sortedArray[index+2] << " , " << sortedArray[index+3] << " ) " << endl;
}		




















