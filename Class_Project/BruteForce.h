/**
* Filename: BruteForce.h
* Purpose: To execute a closest pair search on 'n' units of data that is generated from an external file.
*
* Preconditions: Output'n'.txt where n = the interval (1000, 100000) of pesudo-randomized integers within the interval (-1 000 000, 1 000 000). 
*
* Output: The Closest Pair of coordinates, outputted to the console.
* Contributors: Eric Panich 	- eric.panich@uleth.ca
*				Tylor Faoro 	- tylor.faoro@uleth.ca
*				Juan Monterrosa - juan.monterrosa@uleth.ca
*/

#ifndef BRUTEFORCE_H
#define BRUTEFORCE_H
#include <iostream>		// For cout
#include <math.h>		// For abs
#include <vector>		// For Vectors
#include <climits>		// for INT_MAX
#include <ctime>		// For Clock
#include <chrono>		// For time_point (Run-time clock)
using namespace std;

vector<int> bruteForce();


vector<int> bruteForce(){
	//:: Variables used to determine program run-time.
	chrono::time_point<chrono::system_clock> timeStart, timeEnd;


//	START LOAD VECTOR =========================================================================================
	string filename;
	int size, xCoord, yCoord;
	vector<int> X;
	vector<int> Y;
	
	//:: A set of dummy coordinates to be returned if wrong input is given.
	vector<int> falseCoord = {0,0,0,0};

	cout << "Enter the file you wish to test (Example: Output2000.txt) : ";
	getline(cin, filename);

	ifstream fin;
	fin.open(filename);

	if (!fin.good()) {	
		cout << "bad filename" << endl;
		return falseCoord;
	}
	//First number in file is the number pairs in the algorithm
	fin >> size;
	

	do { 			  //START F.IN
		fin >> xCoord;
		fin >> yCoord;

		X.push_back(xCoord);
		Y.push_back(yCoord);
	}while (!fin.eof());	 //END F.IN

	//END LOAD VECTOR ==========================================================================================

	//:: Start Run-Time Clock.
	timeStart = chrono::system_clock::now();
	
	int minimum = 2000001; // Highest Integer Number
	int distance = 2000000;
	vector<int> coord;
	int index1 = 1, index2 = 1;

	//START BRUTE FORCE
	for (int i = 0; i <= size-2; i++) {
		for (int j = i+1; j <= size-1; j++) {
					
			distance = abs( (X[i]-X[j]) ) + abs( (Y[i]-Y[j]) );
				
			if( distance < minimum) {
				minimum = distance;
				index1 = i;
				index2 = j;
			}
		}
	}
	//END BRUTE FORCE

	//Load coord with index and coord values
	coord.push_back(X[index1]);
	coord.push_back(Y[index1]);
	coord.push_back(index1);
	coord.push_back(X[index2]);
	coord.push_back(Y[index2]);
	coord.push_back(index2);
	
	//:: End Run-Time clock
	timeEnd = chrono::system_clock::now();

	//:: Calculate how long the program ran
	chrono::duration<double> programDuration = timeEnd - timeStart;
	
	//:: Output program Run-Time.
	cout << "Program \"Wall Time\" Duration: " << programDuration.count() << " seconds." << endl << endl; 


	return coord;
}

#endif
